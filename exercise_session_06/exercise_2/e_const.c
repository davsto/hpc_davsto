#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
    // Initialize the MPI environment
    int i, myid, size, n, tag=100, init, part, end;
    double myfact, fact;
    float mysum, sum;
    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank( MPI_COMM_WORLD, &myid);
    if(myid == 0){
       n = 10000;
       part= n/size;
    }
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&part, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if(myid == 0){
        init= 1;
        mysum= 1;
    }
    else{
        init= part*myid;
        mysum= 0;
    }
    end=init+part;
    myfact=1;
    for(i=init; i < end; ++i){
       myfact *= i;
       mysum += 1/myfact;
    }
    if(myid == 0){
        MPI_Send(&fact, 1, MPI_DOUBLE, 1, tag, MPI_COMM_WORLD);
    }
    else if(myid == size-1){
        MPI_Recv(&fact, 1, MPI_DOUBLE, myid-1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        mysum *= 1/fact;
    }
    else{ 
       MPI_Recv(&fact, 1, MPI_DOUBLE, myid-1, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
       mysum *= 1/fact;
       myfact *= fact;
       MPI_Send(&myfact, 1, MPI_DOUBLE, myid+1, tag, MPI_COMM_WORLD);  
    }

    MPI_Reduce(&mysum, &sum, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);
    if (myid == 0){
       printf("the euler constant is approximated by %.16f using N=%d in the sum approximation.\n",sum,n);
    }
    MPI_Finalize();
}
