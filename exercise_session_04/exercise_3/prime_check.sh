#!/bin/bash

if [ -z $1 ]; then
   echo "This is a function to test wether a number is prime."
   echo "Enter number:"
   read x
else
   x=$1
fi

while ! [[ $x =~ ^[0-9]+$ ]] || [[ x -eq 0 ]]; do
   echo "$x is not a valuable input!"
   echo "Wanna quit?(yes/no)"
   read q
   if [ $q == "yes" ]; then
       exit 0
   fi
   echo "Please enter valuable number:"
   read x
done

nr_words=$( factor $x | wc -w )

if [ $nr_words -le 2 ]; then
    echo "Crazy! $x is prime!!!"
else
    echo "Naah, nice try, $x isn't prime!"
fi

exit 0


