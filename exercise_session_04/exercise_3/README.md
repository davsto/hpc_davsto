# Exercise 3 #

In my prime_check.sh function I used the factor function which factorizes a given number in its prime components. Then I just checked the output wether it's greater than two. The factor function is very quick, therefore my bash script is also very quick.

4230283 is a prime number and it took 0.04s to compute it.

4572862171001 is also prime and it took 0.03s to compute it.
