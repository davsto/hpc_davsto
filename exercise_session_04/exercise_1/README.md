# Exercise1 #

Executing the ls -ld $HOME command gives drwx------ as output. This means that only the user can read, write and execute files in this directory. The group and all others can't read, write or execute any file in the $HOME directory. 
In the $SCRATCH directory appart from the user, the group and all others can read and execute files. But only the user can write files in this directory.

If I create a new file in the $HOME and $SCRATCH directories the default permission setting are set to -rw-r--r--, which means the user can read and write, the group and all others can only read.

The permissions for the /users/hlascomb directory are set to rwxr-x--- which means that people in the group can read and execute but not write. The group is uzh4, so for us it is neither possible to read nor write any file because we aren't in this group.

Setting full permissions to only the owner can be done with the command "chmod 700 filename"

A new file created with premissions 000 can't be used. But the user can change the permissions of the file so it isn't lost. 


