#!/bin/bash

if [ -z $1 ]; then
    echo "no input!"
    exit 1
elif ! [ -f $1 ]; then
    echo "$1 doesn't exist"
    exit 1
fi 

while IFS= read -r line; do
   if [ "$line" != "" ]; then
   echo "$line"
   fi    
done < "$1" | awk NF        
exit 0

