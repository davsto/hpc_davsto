#!/bin/bash

if [ -z $1 ]; then
    echo "no input!"
    exit 1
elif ! [ -f $1 ]; then
    echo "$1 doesn't exist!"
    exit 1
fi

while IFS= read -r line; do
    echo -e "$line \n \n \n"
done < "$1"
exit 0


