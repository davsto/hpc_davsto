#!/bin/bash

if [ -f output.txt ]; then
   rm output.txt
fi

touch output.txt

RE="Temperature: ([0-9]+\.[0-9]) deg at time: ([0-9]+\.[0-9]+) sec"
T_sum="0"

while read A ; do 
   if [[ "$A" =~ $RE ]] ; then
      echo "${BASH_REMATCH[1]} ${BASH_REMATCH[2]}" >> output.txt
      T_sum=$(bc <<< "scale=1;$T_sum+${BASH_REMATCH[1]}")
   fi
done
echo "$T_sum"

