#Exercise 1#

The serial version of the cpi code computed pi within 2.2 seconds with an error of ~2*10^(-9).
The serial version of the cpi code computed pi within 4.025 seconds with an error of ~2*10^(-9).
I compiled the cpi_mpi.c file with the cc command because it gave an error whith the gcc compiler.
This could be a reason why the parallelized version took longer.
Another option could be that it had to first initialize and in the end finish the parallelization procedure. 
