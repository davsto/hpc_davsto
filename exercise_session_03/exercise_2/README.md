#Exercise 2#
I compiled everything with the cc command because gcc gave an error.
* With the optimization flag O0 the program took 5.476s to run.
* With the optimization flag O1 the program took 2.363s to run. 
* With the optimization flag O2 the program took 1.974s to run. 
* With the optimization flag O3 the program took 1.959s to run.
I found online the clock() command for C programs, with which one can measure running times.
For the parallelized version of the code I inserted the #pragma omp ... line before the for loop, that's what need to be parallelized.
For the compilation I extended the Makefile and put the flag -O3 for the opitmization.
It ran the code in 1.976s, so it wasn't faster than the serial version, but I don't know why. I ran it with 12 cpu's.   
