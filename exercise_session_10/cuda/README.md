## Exercise 2 ##

The compilation was done as follows: 'nvcc --gpu-architecture=sm_60 -o cpi_cuda cpi_cuda.cu gettime.c'.
The first ten outputs give PI=0. The last two ouputs give the correct PI and are computed in around 0.08 seconds. The times of the first iterations are around 0.0003 seconds.
