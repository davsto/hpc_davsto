## Exercise 2 ##

First I wrote the script map&reduce.sh, which takes all the csv files in the directory and performs map&reduce operation on them.

As a second step I ran the script pymapred.sh to perform map&reduce procedure on the Hadoop system.
There were 7 map operations and 1 reduce operations performed. So each csv file was mapped separately. 
There were 35371 lines/entries treated. An empty _SUCCESS file was created and also a part-00000 file was created where all the word counts are stored. 
