## Exercise 2 ##

Setting N=20 and nsteps=10 we get for the better version: Host version took 0.0222631 s (0.00222631 s/step), GPU version took 0.012701 s (0.0012701 s/step).
For the same parameters we get for the naive version: Host version took 0.0222652 s (0.00222652 s/step), GPU version took 0.0243089 s (0.00243089 s/step).
We see that for large N the better version is double as quick as the naive version.
