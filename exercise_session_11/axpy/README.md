## Exercise 1 ##

Setting N=10 the timings were: openmp: 1.19209e-06 s, openacc: 0.000223875 s. We see that openmp is much quicker in this case. Probably because the copyin and copyout procedure takes a lot of time.

For N=20: openmp: 0.00127006 s, openacc: 0.00247097 s. For this bigger N the openacc is approximately double as quick than the openmp procedure. This means that the gain in computational speed of openacc is more than equalized by the long copyin/copyout procedures.

For N=28: openmp: 0.369968 s, openacc: 0.520814 s. For this N the speedup from N=20 is approximately the same (~2).

We can conclude that the gpu version is double as quick for large N but for small N the copyin/copyout process makes the gpu version very slow in comparison with the computation on the cpu.
